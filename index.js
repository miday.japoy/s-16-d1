
// let count = 5;

// while(count !== 0) {
// 	console.log('Sylvan');
// 	count--;
// }

// let number = 1;

// while(number <= 5) {
// 	console.log(number);
// 	number++;
// }


// let fruits = ['Banana' ,'Mango']

// let i = 0;

// while(i <= 1){ 
// 	console.log(fruits[i]);
// 		i++;
// 	}



// let mobilePhones = ['Samsung Galaxy S21', 'Iphone 13 Pro', 'Xioami 11T', 'Realme C', 'Huawei Nova 8', 'Pixel 5', 'Asus Rog 6', 'Nokia', 'Cherry Mobile'];

// console.log(mobilePhones.length);
// console.log(mobilePhones.length - 1); //will give us the last index position of an element in an array
// console.log(mobilePhones[mobilePhones.length - 1]); //get the last element of an array

// let indexNumberForMobile = 0;

// while(indexNumberForMobile <= mobilePhones.length-1){
// 	console.log(mobilePhones[indexNumberForMobile]);
// 	indexNumberForMobile++;
// }

// let countA = 1;

// do {
// 	console.log("Juan");
// 	countA++;
// } while(countA <= 6);

// let countB = 6;

// do {
// 	console.log(`do-while count ${countB}`)
// 	countB--;
// } while(countB == 7);


// while(countB == 7) {
// 	console.log(`do-while count ${countB}`);
// 	countB--;
// }

// // let indexNumberA = 0;

// let computerBrands = ["a", "h", "A", "l", "aa", "d", "h"];


// // do {

// // 	console.log(computerBrands[indexNumberA]);
// // 	indexNumberA++;

// // } while(indexNumberA <= computerBrands.length - 1);



// for(let int = 0; int <= computerBrands.length - 1; int++){
// 	console.log(computerBrands[int]);
// }	


// let colors = ['red', 'green', 'blue', 'yellow', 'purple', 'white', 'black'];



// for(let x = 0; x <= colors.length - 1; x++){ 
// 	console.log(colors[x]);
// }

// let ages = [18, 19, 20, 21, 24, 25];

// for(let i = 0; i <= ages.length - 1; i++) {
// 	if(ages[i] === 21 || ages === 18){
// 		continue;
// 	}

// 	console.log(ages[i]);
// };







// ASSIGNMENT


//Coding Challenge - Scope (Loops, Continue and Break)
// You can add the solution under our s16/d1/index.js only, no need to create a separate folder for this. Kindly push your solutions once you are done
//Then link your activity gitlab repo under Boodle WD078-16

/*
Instructions:

1. Given the array 'adultAge', display only adult age range on the console.

-- the goal of this activity is to exclude values that are not in the range of adult age. Using the tool loops, continue or break, 
the students must display only the given sample output below on their console.
*/
let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];

/*
	Sample output:
  
  20
  23
  33
  27
  70
  55
  63
  85
*/
for(let i = 0; i <= adultAge.length - 1; i++){
	if(adultAge[i] < 20){
		continue;
	}
	console.log(adultAge[i]);
}

/*
Instructions:

2. Given an array 'students' and a function searchStudent, create a solution that, once a function is invoked with a given name of student as its argument,
	the function will start to loop and search for the student on a given array and once there's found it will print it on the console 
  and stop the execution of loop. 
  
  -- the goal of this activity is to print only the value needed based on the argument given. Use loop, and continue or break
*/
let students = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(studentName){
	//add solutions here
  //you can refer on our last discussion yesterday
  for(let i = 0; i <= students.length; i++){
  	if(students[i] = studentName) {
  		console.log(students[i])
  		break;
  	}
  }
}

searchStudent('Jazz'); //invoked function with a given argument 'Jazz'

/*
	Sample output:
  
  Jazz
*/